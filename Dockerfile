FROM php:8.0-alpine as installer

ARG PHPLOCVERSION=*

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN mkdir -p /tmp/phploc && \
    cd /tmp/phploc && \
	/usr/bin/composer require phploc/phploc:${PHPLOCVERSION}

FROM php:8.0-alpine

RUN mkdir -p /tools
COPY --from=installer /tmp/phploc /tools/phploc
RUN ln -s  /tools/phploc/vendor/bin/phploc /usr/bin/phploc